﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> s = new List<int>();

            int val = 0;
            int i = 0;
            while (val < 4000000)
            {
                val = Fibo(i);
                if (val >= 4000000)
                    break;
                if (val % 2 == 0)
                    s.Add(val);
                i++;
            }

            Console.WriteLine(s.Sum());
            Console.ReadLine();
        }

        static int Fibo(int n)
        {
            if (n == 1)
                return 1;
            if (n > 1)
                return Fibo(n - 1) + Fibo(n - 2);
            return 1;
        }
    }
}
