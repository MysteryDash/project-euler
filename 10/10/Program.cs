﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10
{
    class Program
    {
        static void Main(string[] args)
        {
            long i = 2;
            long primes = 0;
            while (true)
            {
                bool prime = true;
                for (long j = 2; j <= i/2; j++)
                {
                    if (i%j == 0)
                    {
                        prime = false;
                        break;
                    }
                }
                if (i > 2000000)
                    break;
                if (prime)
                {
                    primes += i;
                }
                i++;
            }
            Console.WriteLine(primes);
            Console.Read();
        }
    }
}
