﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _12
{
    class Program
    {
        static void Main(string[] args) // 76576500
        {
            long triangle = 0;
            long i = 1;
            while (true)
            {
                triangle += i;
                i++;

                long divisors = 2; // itself and 1
                for (int j = 2; j <= triangle/2; j++)
                {
                    if (triangle%j == 0)
                    {
                        divisors++;
                    }
                }
                if (divisors > 500)
                {
                    Console.WriteLine(triangle);
                    break;
                }
            }
            Console.Read();
        }
    }
}
