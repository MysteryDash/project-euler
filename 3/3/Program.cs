﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3
{
    class Program
    {
        static void Main(string[] args)
        {
            long x = 600851475143;
            long d = 2;
            while (x > 1)
            {
                while (x%d == 0)
                {
                    x /= d;
                }
                d++;
            }
            Console.WriteLine(d-1);
            Console.ReadLine();
        }
    }
}
