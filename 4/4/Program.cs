﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4
{
    static class Program
    {
        static void Main(string[] args)
        {
            List<int> s = new List<int>();
            for (int i = 1; i <= 999; i++)
                for (int j = 1; j <= 999; j++)
                {
                    var x = (i * j).ToString();
                    if (x == x.Reverse())
                        s.Add(i * j);
                }
            Console.WriteLine(s.Max());
            Console.Read();
        }

        static string Reverse(this string x)
        {
            return new string(x.ToCharArray().Reverse().ToArray());
        }
    }
}
