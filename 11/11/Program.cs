﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _11
{
    class Program
    {
        static void Main(string[] args)
        {
            long product = 0;

            var numbers = new List<int>();
            Properties.Resources.data.Replace("\r\n", " ").Split(' ').ToList().ForEach(x => numbers.Add(Convert.ToInt32(x)));

            // Left - Right
            for (int i = 0; i < numbers.Count - 3; i++)
            {
                long testProduct = 1;
                for (int j = 0; j < 4; j++)
                {
                    testProduct *= numbers[i + j];
                }
                if (testProduct > product)
                    product = testProduct;
            }

            // Diag
            for (int i = 0; i < numbers.Count - 60; i++)
            {
                if (i%20 > 15)
                    continue;
                long testProduct = 1;
                for (int j = 0; j < 4; j++)
                {
                    testProduct *= numbers[i + j*21];
                }
                if (testProduct > product)
                    product = testProduct;
            }

            for (int i = 0; i < numbers.Count - 60; i++)
            {
                if (i%20 < 3)
                    continue;
                long testProduct = 1;
                for (int j = 0; j < 4; j++)
                {
                    testProduct *= numbers[i + j*19];
                }
                if (testProduct > product)
                    product = testProduct;
            }

            // Up - Down
            for (int i = 0; i < numbers.Count - 60; i++)
            {
                long testProduct = 1;
                for (int j = 0; j < 4; j++)
                {
                    testProduct *= numbers[i + j*20];
                }
                if (testProduct > product)
                    product = testProduct;
            }

            Console.WriteLine(product);
            Console.ReadLine();
        }
    }
}
