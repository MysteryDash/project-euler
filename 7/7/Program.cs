﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _7
{
    class Program
    {
        static void Main(string[] args)
        {
            long i = 2;
            long count = 0;
            while (true)
            {
                bool prime = true;
                for (long j = 2; j < i; j++)
                    if (i%j == 0)
                    {
                        prime = false;
                        break;
                    }
                if (prime) count++;
                if (count == 10001)
                {
                    Console.WriteLine(i);
                    Console.ReadLine();
                    break;
                }
                i++;
            }
        }
    }
}
