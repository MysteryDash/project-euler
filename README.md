# Project Euler #

Here are C# answers for some Project Euler's challenge.

### Contribution guidelines ###

* Fork this repository
* Add your answer to a challenge that hasn't already be done (or update an answer with a more efficient program)
* The name of the directory must correspond to the challenge's number
* Don't forget to add yourself to the contribution list
* Create a pull request

### Contributions ###

* [MysteryDash](https://bitbucket.org/MysteryDash/)

### License ###

This project is licensed under the MIT license.