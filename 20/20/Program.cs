﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using System.Runtime.Remoting.Messaging;
using System.Security.Cryptography.X509Certificates;

namespace _20
{
    class Program
    {
        static void Main(string[] args)
        {
            var n = new BigInteger(100);
            for (int i = 99; i > 1; i--)
                n *= i;
            Console.WriteLine(n.ToString().ToCharArray().ToList().Sum(c => c - 48));
            Console.Read();
        }
    }
}
