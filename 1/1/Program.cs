﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> n = new List<int>();
            for (int i = 1; i < 1000; i++)
            {
                if (i%3 == 0 || i%5 == 0)
                    n.Add(i);
            }
            Console.WriteLine(n.Sum());
            Console.ReadLine();
        }
    }
}
