﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6
{
    class Program
    {
        static void Main(string[] args)
        {
            var su = new List<int>();
            var sq = new List<int>();
            for (int i = 1; i <= 100; i++)
            {
                su.Add(i*i);
                sq.Add(i);
            }
            Console.WriteLine((sq.Sum()*sq.Sum()) - su.Sum());
            Console.Read();
        }
    }
}
