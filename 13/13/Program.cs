﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace _13
{
    class Program
    {
        static void Main(string[] args)
        {
            var result = new BigInteger(0);
            Properties.Resources.data.Replace("\r\n", " ")
                .Split(' ')
                .ToList()
                .ForEach(x => result += BigInteger.Parse(x));
            Console.WriteLine(result);
            
            Console.Read();
        }
    }
}
