﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _14
{
    class Program
    {
        static void Main(string[] args)
        {
            long i = 2;
            long longestI = 0;
            long longestChain = 0;
            while (i < 1000000)
            {
                long x = i;
                long chain = 0;
                while (x > 1)
                {
                    if (x%2 == 0)
                        x /= 2;
                    else
                        x = 3*x + 1;
                    chain++;
                }
                if (chain > longestChain)
                {
                    longestChain = chain;
                    longestI = i;
                }
                i++;
            }
            Console.WriteLine(longestI);
            Console.WriteLine(longestChain);
            Console.Read();
        }
    }
}
