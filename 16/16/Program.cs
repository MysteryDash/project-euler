﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace _16
{
    class Program
    {
        static void Main(string[] args)
        {
            var n = BigInteger.Pow(2, 1000);
            var l = n.ToString().ToCharArray();
            var ll = new List<int>();
            l.ToList().ForEach(x => ll.Add(x - 48));
            Console.WriteLine(ll.Sum());
            Console.Read();
        }
    }
}
