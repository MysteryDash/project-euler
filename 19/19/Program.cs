﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace _19
{
    class Program
    {
        static void Main(string[] args)
        {
            var date = new DateTime(1901, 1, 1);
            int sundays = 0;
            while (date.Year < 2001)
            {
                date = date.AddMonths(1);
                if (date.DayOfWeek == DayOfWeek.Sunday)
                    sundays++;
            }
            Console.WriteLine(sundays);
            Console.Read();
        }
    }
}
